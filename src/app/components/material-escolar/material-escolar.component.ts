import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-material-escolar',
  templateUrl: './material-escolar.component.html',
  styleUrls: ['./material-escolar.component.css']
})
export class MaterialEscolarComponent implements OnInit {

  form!: FormGroup;
  form1!: FormGroup;
  lista!: string;
  entra: string[] = [];
  lista2!: string;

  constructor(
    private fb: FormBuilder

  ) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
  }


  crearFormulario(): void {
    this.form = this.fb.group(
      {
        id: [''],
        // aceptar: ['false'],
        utiles: this.fb.array([[this.form1 = this.fb.group({
          aceptar: ['', Validators.requiredTrue],
          nombre: ['',  [Validators.required, Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
        })
        ]]),
      }
    )
  }

  get utilesEscolares () {
    return this.form.get('utiles') as FormArray;
  }

  get nombreNoValido(){
    return this.form1.get('nombre')?.invalid && this.form1.get('nombre')?.touched
  }

  agregarUtil(): void {
    this.utilesEscolares.push(this.fb.control(''))
    this.form1.reset({
      nombre:''
    })

   
  }

  borrarUtil(id: number): void {
    this.utilesEscolares.removeAt(id)
  }

  eliminarUtiles(): void {
    // volvemos los valores a un array vacio
    this.form = this.fb.group(
      {
        utiles: this.fb.array([])
      }
    )
  }

  limpiar(): void { 
    this.entra = []
    this.lista2 = this.entra.toString()
    // volvemos el valor de los input vacias  
   this.form1.reset({
     nombre: ''
   })
   
  }


 guardar(): void {

    // con esto vamo a verificamos si el checkbox esta seleccionado
    if (this.form1.value.aceptar === true) {

      //Para obtener el valor del nombre del input
      this.lista = this.form1.value.nombre;

      // Para guardarlo en un array cada nombre
      this.entra.push(this.lista)
      
      // convertimos el array en un string
      // y lo separamos con un salto de linea
      this.lista2 = this.entra.join('\n')
    }
  }
}
